#+TITLE: September 2-17, 2020 (16 days)
#+PROPERTY: Effort_ALL 0 0:05 0:10 0:30 1:00 2:00 3:00 4:00
#+COLUMNS: %35ITEM %TASKID %OWNER %3PRIORITY %TODO %5ESTIMATED{+} %3ACTUAL{+}
* REPORTS
** SCRUM BOARD
#+BEGIN: block-update-board
#+END:
** DEVELOPER SUMMARY
#+BEGIN: block-update-summary
#+END:
** BURNDOWN CHART
#+BEGIN: block-update-graph
#+END:
** BURNDOWN LIST
#+PLOT: title:"Burndown" ind:1 deps:(3 4) set:"term dumb" set:"xtics scale 0.5" set:"ytics scale 0.5" file:"burndown.plt" set:"xrange [0:17]"
#+BEGIN: block-update-burndown
#+END:
** TASK LIST
#+BEGIN: columnview :hlines 2 :maxlevel 5 :id "TASKS"
#+END:
* TASKS
  :PROPERTIES:
  :ID:       TASKS
  :SPRINTLENGTH: 16
  :SPRINTSTART: <2020-09-02 Wed>
  :wpd-akshay196: 1
  :wpd-bhavin192: 1
  :wpd-gandalfdwite: 1
  :wpd-jasonbraganza: 5
  :wpd-sandeepk: 1
  :END:
** akshay196
*** DONE GopherCon 2019 - Lightning Talks
    CLOSED: [2020-09-17 Thu 09:28]
   :PROPERTIES:
   :ESTIMATED: 6
   :ACTUAL:   6.90
   :OWNER:    akshay196
   :ID:       READ.1599058358
   :TASKID:   READ.1599058358
   :END:
   :LOGBOOK:
   CLOCK: [2020-09-17 Thu 08:35]--[2020-09-17 Thu 09:28] =>  0:53
   CLOCK: [2020-09-16 Wed 08:12]--[2020-09-16 Wed 09:01] =>  0:49
   CLOCK: [2020-09-15 Tue 08:20]--[2020-09-15 Tue 08:48] =>  0:28
   CLOCK: [2020-09-14 Mon 22:14]--[2020-09-14 Mon 22:53] =>  0:39
   CLOCK: [2020-09-14 Mon 20:04]--[2020-09-14 Mon 20:40] =>  0:36
   CLOCK: [2020-09-14 Mon 08:51]--[2020-09-14 Mon 09:17] =>  0:26
   CLOCK: [2020-09-13 Sun 11:39]--[2020-09-13 Sun 13:37] =>  1:58
   CLOCK: [2020-09-05 Sat 12:11]--[2020-09-05 Sat 13:16] =>  1:05
   :END:
   https://www.youtube.com/playlist?list=PL2ntRZ1ySWBedT1MDWF4xAD39vAad0DBT

** bhavin192
*** DONE Site Reliability Engineering - II. Principles - Part III [3/3]
    CLOSED: [2020-09-10 Thu 20:17]
    :PROPERTIES:
    :ESTIMATED: 9
    :ACTUAL:   8.47
    :OWNER:    bhavin192
    :ID:       READ.1596455869
    :TASKID:   READ.1596455869
    :END:
    :LOGBOOK:
    CLOCK: [2020-09-10 Thu 20:00]--[2020-09-10 Thu 20:17] =>  0:17
    CLOCK: [2020-09-10 Thu 19:09]--[2020-09-10 Thu 19:45] =>  0:36
    CLOCK: [2020-09-09 Wed 21:48]--[2020-09-09 Wed 22:16] =>  0:28
    CLOCK: [2020-09-08 Tue 20:13]--[2020-09-08 Tue 20:40] =>  0:27
    CLOCK: [2020-09-08 Tue 19:19]--[2020-09-08 Tue 20:03] =>  0:44
    CLOCK: [2020-09-07 Mon 20:16]--[2020-09-07 Mon 21:17] =>  1:01
    CLOCK: [2020-09-06 Sun 18:17]--[2020-09-06 Sun 18:47] =>  0:30
    CLOCK: [2020-09-06 Sun 18:04]--[2020-09-06 Sun 18:14] =>  0:10
    CLOCK: [2020-09-06 Sun 17:32]--[2020-09-06 Sun 17:55] =>  0:23
    CLOCK: [2020-09-06 Sun 16:47]--[2020-09-06 Sun 17:20] =>  0:33
    CLOCK: [2020-09-05 Sat 20:55]--[2020-09-05 Sat 21:20] =>  0:25
    CLOCK: [2020-09-05 Sat 19:58]--[2020-09-05 Sat 20:51] =>  0:53
    CLOCK: [2020-09-04 Fri 23:30]--[2020-09-04 Fri 23:40] =>  0:10
    CLOCK: [2020-09-03 Thu 18:43]--[2020-09-03 Thu 19:46] =>  1:03
    CLOCK: [2020-09-02 Wed 21:53]--[2020-09-02 Wed 22:25] =>  0:32
    CLOCK: [2020-09-02 Wed 20:43]--[2020-09-02 Wed 20:59] =>  0:16
    :END:
    https://landing.google.com/sre/sre-book/toc/index.html
    - [X] 7. The Evolution of Automation at Google - II                            (4h)
    - [X] 8. Release Engineering                                                   (3h)
    - [X] 9. Simplicity                                                            (2h)
*** DONE Site Reliability Engineering - III. Practices - Part I [2/2]
    CLOSED: [2020-09-17 Thu 19:58]
    :PROPERTIES:
    :ESTIMATED: 7
    :ACTUAL:   6.57
    :OWNER:    bhavin192
    :ID:       READ.1596455869
    :TASKID:   READ.1596455869
    :END:
    :LOGBOOK:
    CLOCK: [2020-09-17 Thu 19:15]--[2020-09-17 Thu 19:58] =>  0:43
    CLOCK: [2020-09-16 Wed 20:28]--[2020-09-16 Wed 21:28] =>  1:00
    CLOCK: [2020-09-15 Tue 20:48]--[2020-09-15 Tue 21:01] =>  0:13
    CLOCK: [2020-09-14 Mon 20:39]--[2020-09-14 Mon 21:15] =>  0:36
    CLOCK: [2020-09-14 Mon 20:14]--[2020-09-14 Mon 20:39] =>  0:25
    CLOCK: [2020-09-13 Sun 20:01]--[2020-09-13 Sun 20:22] =>  0:21
    CLOCK: [2020-09-13 Sun 19:01]--[2020-09-13 Sun 19:42] =>  0:41
    CLOCK: [2020-09-13 Sun 17:11]--[2020-09-13 Sun 18:04] =>  0:53
    CLOCK: [2020-09-13 Sun 16:55]--[2020-09-13 Sun 17:05] =>  0:10
    CLOCK: [2020-09-12 Sat 22:19]--[2020-09-12 Sat 23:36] =>  1:17
    CLOCK: [2020-09-12 Sat 20:34]--[2020-09-12 Sat 20:49] =>  0:15
    :END:
    https://landing.google.com/sre/sre-book/toc/index.html
    - [X] 10. Practical Alerting from Time-Series Data                             (4h)
    - [X] 11. Being On-Call                                                        (3h)

** gandalfdwite
*** DONE Git Pocket Guide [5/5]
    CLOSED: [2020-09-18 Fri 20:14]
    :PROPERTIES:
    :ESTIMATED: 11
    :ACTUAL:   11.02
    :OWNER: gandalfdwite
    :ID: READ.1593841309
    :TASKID: READ.1593841309
    :END:
    :LOGBOOK:
    CLOCK: [2020-09-17 Thu 18:00]--[2020-09-17 Thu 19:10] =>  1:10
    CLOCK: [2020-09-16 Wed 23:00]--[2020-09-17 Thu 00:10] =>  1:10
    CLOCK: [2020-09-15 Tue 22:45]--[2020-09-15 Tue 23:50] =>  1:05
    CLOCK: [2020-09-13 Sun 16:10]--[2020-09-13 Sun 17:15] =>  1:05
    CLOCK: [2020-09-12 Sat 11:00]--[2020-09-12 Sat 12:46] =>  1:46
    CLOCK: [2020-09-10 Thu 23:15]--[2020-09-11 Fri 00:25] =>  1:10
    CLOCK: [2020-09-09 Wed 21:05]--[2020-09-09 Wed 22:25] =>  1:20
    CLOCK: [2020-09-06 Sun 19:05]--[2020-09-06 Sun 20:00] =>  0:55
    CLOCK: [2020-09-05 Sat 16:30]--[2020-09-05 Sat 17:50] =>  1:20
    :END:
    - [X] Understanding Git     ( 2h )
    - [X] Getting started       ( 2h )
    - [X] Making Commits        ( 2h )
    - [X] Undoing & Editing     ( 2h )
    - [X] Branching             ( 3h )

*** DONE Container Networking for Docker & Kubernetes [1/1]
    CLOSED: [2020-09-05 Sat 10:02]
    :PROPERTIES:
    :ESTIMATED: 5
    :ACTUAL:   5.03
    :OWNER: gandalfdwite
    :ID: READ.1599039494
    :TASKID: READ.1599039494
    :END:
    :LOGBOOK:
    CLOCK: [2020-09-05 Sat 08:50]--[2020-09-05 Sat 10:00] =>  1:10
    CLOCK: [2020-09-04 Fri 22:03]--[2020-09-04 Fri 23:05] =>  1:02
    CLOCK: [2020-09-03 Thu 23:00]--[2020-09-04 Fri 00:45] =>  1:45
    CLOCK: [2020-09-02 Wed 21:25]--[2020-09-02 Wed 22:30] =>  1:05
    :END:
    - [X] Container Networking e-book by Oreilly ( 5h )

** jasonbraganza
*** IN_PROGRESS Complete all of Reuven Lerner’s exercises courses + books - Part I [50%]
    :PROPERTIES:
    :ESTIMATED: 13
    :ACTUAL:   4.12
    :OWNER: jasonbraganza
    :ID: DEV.1597757019
    :TASKID: DEV.1597757019
    :END:
    :LOGBOOK:
    CLOCK: [2020-09-10 Thu 12:02]--[2020-09-10 Thu 12:37] =>  0:35
    CLOCK: [2020-09-09 Wed 16:15]--[2020-09-09 Wed 18:07] =>  1:52
    CLOCK: [2020-09-09 Wed 12:00]--[2020-09-09 Wed 12:11] =>  0:11
    CLOCK: [2020-09-08 Tue 10:40]--[2020-09-08 Tue 11:31] =>  0:51
    CLOCK: [2020-09-04 Fri 10:15]--[2020-09-04 Fri 10:53] =>  0:38
    :END:
**** DONE WORKING advanced functions (Total 2) [1/1]
     CLOSED: [2020-09-04 Fri 10:53]
     - [X] Exercise 3
**** TODO advanced python objects (Total 10) [4/10]
     - [X] Exercise 1
     - [X] Exercise 2
     - [X] Exercise 3
     - [X] Exercise 4
     - [ ] Exercise 5
     - [ ] Exercise 6
     - [ ] Exercise 7
     - [ ] Exercise 8
     - [ ] Exercise 9
     - [ ] Exercise 10
*** IN_PROGRESS Complete Miguel Grinberg Flask Course (1 hour a day) - Part II [0%]
    :PROPERTIES:
    :ESTIMATED: 7
    :ACTUAL:   0.27
    :OWNER: jasonbraganza
    :ID: DEV.1599141548
    :TASKID: DEV.1599141548
    :END:
    :LOGBOOK:
    CLOCK: [2020-09-08 Tue 11:40]--[2020-09-08 Tue 11:56] =>  0:16
    :END:
    - [-] Web Forms [1/2]
      - [X] Improving Field Validation
      - [ ] Generating Links
    - [ ] Database [0/4]
      - [ ] Databases in Flask
      - [ ] Database Migrations
      - [ ] Flask-SQLAlchemy Configuration
      - [ ] Database Models
*** DONE BEVAE 181 - Environmental Studies - Part I [100%]
    CLOSED: [2020-09-08 Tue 09:34]
   :PROPERTIES:
   :ESTIMATED: 3
   :ACTUAL:   1.63
   :OWNER: jasonbraganza
   :ID: READ.1597635452
   :TASKID: READ.1597635452
   :END:
   :LOGBOOK:
   CLOCK: [2020-09-08 Tue 08:45]--[2020-09-08 Tue 09:34] =>  0:49
   CLOCK: [2020-09-04 Fri 09:16]--[2020-09-04 Fri 09:24] =>  0:08
   CLOCK: [2020-09-04 Fri 08:19]--[2020-09-04 Fri 09:00] =>  0:41
   :END:
   - [X] Unit 7 - Energy Resources
   - [X] BLOCK 3 - Environmental Issues and Concerns (2020/08/31 - 2020/09/01)
   - [X] Unit 8 - Biodiversity: Threats and Conservation
*** DONE BEVAE 181 - Environmental Studies - Part II [100%]
    CLOSED: [2020-09-15 Tue 11:30]
    :PROPERTIES:
    :ESTIMATED: 7
    :ACTUAL:   4.77
    :OWNER: jasonbraganza
    :ID: READ.1599140146
    :TASKID: READ.1599140146
    :END:
    :LOGBOOK:
    CLOCK: [2020-09-15 Tue 10:00]--[2020-09-15 Tue 11:27] =>  1:27
    CLOCK: [2020-09-14 Mon 11:34]--[2020-09-14 Mon 12:57] =>  1:23
    CLOCK: [2020-09-11 Fri 07:13]--[2020-09-11 Fri 08:00] =>  0:47
    CLOCK: [2020-09-10 Thu 07:21]--[2020-09-10 Thu 08:17] =>  0:56
    CLOCK: [2020-09-09 Wed 06:25]--[2020-09-09 Wed 06:38] =>  0:13
    :END:
    - [X] Unit 9 - Environmental Pollution & Hazards
    - [X] Unit 10 - Waste Management
    - [X] Unit 11 - Global Environmental Issues
    - [X] BLOCK 4 - Protecting Our Environment: Policies and Practices (2020/09/07 - 2020/09/11)
    - [X] Unit 12 - Environmental Legislation
    - [X] Unit 13 - Human Communities and Environment
    - [X] Unit 14 - Environmental Ethics
*** DONE BHIC 131 - History of India from the Earliest Times - Part II [100%]
    CLOSED: [2020-09-10 Thu 09:05]
    :PROPERTIES:
    :ESTIMATED: 6
    :ACTUAL:   3.23
    :OWNER: jasonbraganza
    :ID: READ.1597641618
    :TASKID: READ.1597641618
    :END:
    :LOGBOOK:
    CLOCK: [2020-09-10 Thu 08:20]--[2020-09-10 Thu 09:05] =>  0:45
    CLOCK: [2020-09-09 Wed 07:41]--[2020-09-09 Wed 08:58] =>  1:17
    CLOCK: [2020-09-08 Tue 10:18]--[2020-09-08 Tue 10:36] =>  0:18
    CLOCK: [2020-09-08 Tue 09:34]--[2020-09-08 Tue 10:00] =>  0:26
    CLOCK: [2020-09-04 Fri 09:24]--[2020-09-04 Fri 09:52] =>  0:28
    :END:
    - [X] Unit 07 - Chalcolithic and Early Iron Age (2020/09/02 - 2020/09/03)
    - [X] Unit 08 - The Early Vedic Society (2020/09/04 - 2020/09/07)
    - [X] Unit 09 - Changes in the Later Vedic Phase (2020/09/08 - 2020/09/09)
    - [X] Unit 10 - Janapadas and Mahajanapadas: Rise of Urban Centres, Society and Economy (2020/09/10 - 2020/09/11)
    - [X] Unit 11 - Buddhism, Jainism and other Religious Ideas (2020/09/14 - 2020/09/15)
    - [X] Unit 12 - Alexander’s Invasion (2020/09/16 - 2020/09/17)
*** DONE BHIC 131 - History of India from the Earliest Times - Part III [100%]
    CLOSED: [2020-09-15 Tue 09:30]
    :PROPERTIES:
    :ESTIMATED: 4
    :ACTUAL:   3.23
    :OWNER: jasonbraganza
    :ID: READ.1599140359
    :TASKID: READ.1599140359
    :END:
    :LOGBOOK:
    CLOCK: [2020-09-15 Tue 08:01]--[2020-09-15 Tue 09:30] =>  1:29
    CLOCK: [2020-09-14 Mon 13:30]--[2020-09-14 Mon 14:15] =>  0:45
    CLOCK: [2020-09-11 Fri 09:15]--[2020-09-11 Fri 09:42] =>  0:27
    CLOCK: [2020-09-11 Fri 08:37]--[2020-09-11 Fri 08:46] =>  0:09
    CLOCK: [2020-09-11 Fri 08:16]--[2020-09-11 Fri 08:19] =>  0:03
    CLOCK: [2020-09-10 Thu 09:05]--[2020-09-10 Thu 09:26] =>  0:21
    :END:
    - [X] Unit 13 - Establishment of Mauryan Rule and Magadhan Territorial Expansion (2020/09/18 - 2020/09/21)
    - [X] Unit 14 - Administrative Organization, Economy and Society (2020/09/22 - 2020/09/23)
    - [X] Unit 15 - Early State Formation in Deccan and Tamilaham (2020/09/24 - 2020/09/25)
    - [X] Unit 16 - Agrarian Settlements, Agrarian Society, Expansion of Trade and Urban Centres – Peninsular India (2020/09/28 - 2020/09/29)
*** DONE Complete Mentor Assignments (1 hour a day) - Part I [100%]
    CLOSED: [2020-09-16 Wed 10:05]
    :PROPERTIES:
    :ESTIMATED: 13
    :ACTUAL:   19.13
    :OWNER:    jasonbraganza
    :ID:       DEV.1597642007
    :TASKID:   DEV.1597642007
    :END:
    :LOGBOOK:
    CLOCK: [2020-09-16 Wed 13:55]--[2020-09-16 Wed 18:40] =>  4:45
    CLOCK: [2020-09-16 Wed 12:05]--[2020-09-16 Wed 12:40] =>  0:35
    CLOCK: [2020-09-16 Wed 11:02]--[2020-09-16 Wed 11:22] =>  0:20
    CLOCK: [2020-09-16 Wed 07:50]--[2020-09-16 Wed 10:15] =>  2:25
    CLOCK: [2020-09-15 Tue 14:48]--[2020-09-15 Tue 17:00] =>  2:12
    CLOCK: [2020-09-14 Mon 15:00]--[2020-09-14 Mon 16:15] =>  1:15
    CLOCK: [2020-09-11 Fri 11:20]--[2020-09-11 Fri 12:24] =>  1:04
    CLOCK: [2020-09-11 Fri 10:20]--[2020-09-11 Fri 11:04] =>  0:44
    CLOCK: [2020-09-10 Thu 10:02]--[2020-09-10 Thu 12:02] =>  2:00
    CLOCK: [2020-09-09 Wed 10:10]--[2020-09-09 Wed 11:47] =>  1:37
    CLOCK: [2020-09-08 Tue 17:00]--[2020-09-08 Tue 18:00] =>  1:00
    CLOCK: [2020-09-04 Fri 11:50]--[2020-09-04 Fri 12:52] =>  1:02
    CLOCK: [2020-09-04 Fri 11:36]--[2020-09-04 Fri 11:45] =>  0:09
    :END:
     - [X] Day 5 Write a tool, which will take an url as input, and download
                 all the images from that url/webpage
                 (https://www.crummy.com/software/BeautifulSoup/bs4/doc/)
     - [X] Day 6
     - [X] Day 7
     - [X] Day 8
     - [X] Day 9
     - [X] Day 10
     - [X] Day 11
     - [X] Day 12
     - [X] Day 13
*** DONE Complete Miguel Grinberg Flask Course (1 hour a day) - Part I [100%]
    CLOSED: [2020-09-04 Fri 11:35]
    :PROPERTIES:
    :ESTIMATED: 2
    :ACTUAL:   0.43
    :OWNER: jasonbraganza
    :ID: DEV.1597751735
    :TASKID: DEV.1597751735
    :END:
    :LOGBOOK:
    CLOCK: [2020-09-04 Fri 11:14]--[2020-09-04 Fri 11:35] =>  0:21
    CLOCK: [2020-09-04 Fri 10:55]--[2020-09-04 Fri 11:00] =>  0:05
    :END:
    - [X] Web Forms [2/2]
      - [X] Form Views
      - [X] Receiving Form Data
** sandeepk
*** DONE Practical Python Programming [6/6]
    CLOSED: [2020-09-10 Thu 23:28]
   :PROPERTIES:
   :ESTIMATED: 17
   :ACTUAL:   7.55
   :OWNER: sandeepk
   :ID: READ.1598776776
   :TASKID: READ.1598776776
   :END:
   :LOGBOOK:
   CLOCK: [2020-09-10 Thu 22:17]--[2020-09-10 Thu 23:27] =>  1:10
   CLOCK: [2020-09-09 Wed 21:17]--[2020-09-09 Wed 22:45] =>  1:28
   CLOCK: [2020-09-08 Tue 22:30]--[2020-09-08 Tue 23:20] =>  0:50
   CLOCK: [2020-09-07 Mon 22:20]--[2020-09-07 Mon 23:00] =>  0:40
   CLOCK: [2020-09-06 Sun 19:00]--[2020-09-06 Sun 20:00] =>  1:00
   CLOCK: [2020-09-04 Fri 22:50]--[2020-09-04 Fri 23:50] =>  1:00
   CLOCK: [2020-09-03 Thu 23:20]--[2020-09-03 Thu 23:45] =>  0:25
   CLOCK: [2020-09-02 Wed 21:33]--[2020-09-02 Wed 22:33] =>  1:02
   :END:
   - [X] Chapter 4 Classes         ( 180m )
   - [X] Chapter 5 Inner Working   ( 120m )
   - [X] Chapter 6 Generatore      ( 240m )
   - [X] Chapter 7 Advance Topics  ( 300m )
   - [X] Chapter 8 Testing         ( 120m )
   - [X] Chapter 9 Packages        ( 60m )

*** IN_PROGRESS Wagtail [100%]
    CLOSED: [2020-09-17 Thu 22:40]
   :PROPERTIES:
   :ESTIMATED: 4 
   :ACTUAL:   5.17
   :OWNER: sandeepk
   :ID: DEV.1598776989
   :TASKID: DEV.1598776989
   :END:
   :LOGBOOK:
   CLOCK: [2020-09-17 Thu 21:50]--[2020-09-17 Thu 22:40] =>  0:50
   CLOCK: [2020-09-16 Wed 21:30]--[2020-09-16 Wed 22:40] =>  1:10
   CLOCK: [2020-09-15 Tue 22:00]--[2020-09-15 Tue 22:50] =>  0:50
   CLOCK: [2020-09-14 Mon 22:00]--[2020-09-14 Mon 22:40] =>  0:40
   CLOCK: [2020-09-12 Sat 20:00]--[2020-09-12 Sat 20:40] =>  0:40
   CLOCK: [2020-09-12 Sat 16:00]--[2020-09-12 Sat 17:00] =>  1:00
   :END:
   - [X] Skim through project documentation  ( 40m )
   - [X] Local Setup of the project          ( 100m )

