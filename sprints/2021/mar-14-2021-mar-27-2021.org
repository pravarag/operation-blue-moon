#+TITLE: March 14-27, 2021 (14 days)
#+PROPERTY: Effort_ALL 0 0:05 0:10 0:30 1:00 2:00 3:00 4:00
#+COLUMNS: %35ITEM %TASKID %OWNER %3PRIORITY %TODO %5ESTIMATED{+} %3ACTUAL{+}
* REPORTS
** SCRUM BOARD
#+BEGIN: block-update-board
#+END:
** DEVELOPER SUMMARY
#+BEGIN: block-update-summary
#+END:
** BURNDOWN CHART
#+BEGIN: block-update-graph
#+END:
** BURNDOWN LIST
#+PLOT: title:"Burndown" ind:1 deps:(3 4) set:"term dumb" set:"xtics scale 0.5" set:"ytics scale 0.5" file:"burndown.plt" set:"xrange [0:17]"
#+BEGIN: block-update-burndown
#+END:
** TASK LIST
#+BEGIN: columnview :hlines 2 :maxlevel 5 :id "TASKS"
#+END:
* TASKS
  :PROPERTIES:
  :ID:       TASKS
  :SPRINTLENGTH: 14
  :SPRINTSTART: <2021-03-14 Sun>
  :wpd-akshay196: 1
  :wpd-bhavin192: 1
  :wpd-gandalfdwite: 1
  :wpd-yudocaa: 1
  :END:
** akshay196
*** DONE Solve Go Examples - Part VI
    CLOSED: [2021-03-27 Sat 12:52]
    :PROPERTIES:
    :ESTIMATED: 14
    :ACTUAL:   11.33
    :OWNER: akshay196
    :ID: DEV.1609175417
    :TASKID: DEV.1609175417
    :END:
    :LOGBOOK:
    CLOCK: [2021-03-27 Sat 11:40]--[2021-03-27 Sat 12:52] =>  1:12
    CLOCK: [2021-03-26 Fri 07:46]--[2021-03-26 Fri 08:40] =>  0:54
    CLOCK: [2021-03-25 Thu 07:57]--[2021-03-25 Thu 08:53] =>  0:56
    CLOCK: [2021-03-24 Wed 08:21]--[2021-03-24 Wed 09:01] =>  0:40
    CLOCK: [2021-03-23 Tue 08:01]--[2021-03-23 Tue 09:04] =>  1:03
    CLOCK: [2021-03-22 Mon 08:15]--[2021-03-22 Mon 09:00] =>  0:45
    CLOCK: [2021-03-20 Sat 09:17]--[2021-03-20 Sat 09:40] =>  0:23
    CLOCK: [2021-03-19 Fri 07:56]--[2021-03-19 Fri 08:53] =>  0:57
    CLOCK: [2021-03-18 Thu 08:06]--[2021-03-18 Thu 08:53] =>  0:47
    CLOCK: [2021-03-17 Wed 18:10]--[2021-03-17 Wed 19:53] =>  1:43
    CLOCK: [2021-03-16 Tue 09:11]--[2021-03-16 Tue 10:19] =>  1:08
    CLOCK: [2021-03-15 Mon 20:36]--[2021-03-15 Mon 21:28] =>  0:52
    :END:
    - [X] 19-strings-runes-bytes                      (2h)
    - [X] 20-project-spam-masker                      (3h)
    - [X] 21-project-text-wrapper                     (2h)
    - [X] 22-maps                                     (3h)
    - [X] 23-input-scanning                           (4h)
** bhavin192
*** DONE Site Reliability Engineering - IV. Management - Part II [2/2]
    CLOSED: [2021-03-23 Tue 20:54]
    :PROPERTIES:
    :ESTIMATED: 5
    :ACTUAL:   4.50
    :OWNER:    bhavin192
    :ID:       READ.1596455869
    :TASKID:   READ.1596455869
    :END:
    :LOGBOOK:
    CLOCK: [2021-03-23 Tue 20:24]--[2021-03-23 Tue 20:54] =>  0:30
    CLOCK: [2021-03-22 Mon 20:15]--[2021-03-22 Mon 21:15] =>  1:00
    CLOCK: [2021-03-21 Sun 14:02]--[2021-03-21 Sun 14:20] =>  0:18
    CLOCK: [2021-03-20 Sat 20:47]--[2021-03-20 Sat 21:02] =>  0:15
    CLOCK: [2021-03-20 Sat 20:15]--[2021-03-20 Sat 20:40] =>  0:25
    CLOCK: [2021-03-18 Thu 21:54]--[2021-03-18 Thu 22:02] =>  0:08
    CLOCK: [2021-03-18 Thu 21:41]--[2021-03-18 Thu 21:51] =>  0:10
    CLOCK: [2021-03-18 Thu 20:42]--[2021-03-18 Thu 21:00] =>  0:18
    CLOCK: [2021-03-16 Tue 20:42]--[2021-03-16 Tue 21:10] =>  0:28
    CLOCK: [2021-03-15 Mon 22:35]--[2021-03-15 Mon 22:51] =>  0:16
    CLOCK: [2021-03-15 Mon 20:27]--[2021-03-15 Mon 21:09] =>  0:42
    :END:
    https://landing.google.com/sre/sre-book/toc/index.html
    - [X] 29. Dealing with Interrupts					(3h)
    - [X] 30. Embedding an SRE to Recover from Operational Overload     (2h)
*** DONE Watch FOSDEM 2021 - Containers - Part II [5/5]
    CLOSED: [2021-03-21 Sun 18:29]
    :PROPERTIES:
    :ESTIMATED: 2.5
    :ACTUAL:   1.80
    :OWNER:    bhavin192
    :ID:       READ.1614970632
    :TASKID:   READ.1614970632
    :END:
    :LOGBOOK:
    CLOCK: [2021-03-21 Sun 18:07]--[2021-03-21 Sun 18:29] =>  0:22
    CLOCK: [2021-03-21 Sun 17:48]--[2021-03-21 Sun 18:02] =>  0:14
    CLOCK: [2021-03-21 Sun 17:24]--[2021-03-21 Sun 17:41] =>  0:17
    CLOCK: [2021-03-21 Sun 16:53]--[2021-03-21 Sun 17:02] =>  0:09
    CLOCK: [2021-03-21 Sun 16:09]--[2021-03-21 Sun 16:35] =>  0:26
    CLOCK: [2021-03-20 Sat 22:46]--[2021-03-20 Sat 23:06] =>  0:20
    :END:
    https://fosdem.org/2021/
    - [X] [[https://fosdem.org/2021/schedule/event/containers_toolbox/][By The Power of toolbox!]]                                            (30m)
    - [X] [[https://fosdem.org/2021/schedule/event/containers_lxd_cluster/][Fully redundant LXD cluster]]                                         (35m)
    - [X] [[https://fosdem.org/2021/schedule/event/containers_lazy_pull/][Build and Run Containers With Lazy Pulling]]                          (30m)
    - [X] [[https://fosdem.org/2021/schedule/event/containers_darwin_containerd/][containerd port to darwin: Toward Running Linux containers on macOS]] (30m)
    - [X] [[https://fosdem.org/2021/schedule/event/containers_datacenter_class/][Datacenter class containers for the masses]]                          (25m)
*** DONE Watch FOSDEM 2021 - Infra Management [9/9]
    CLOSED: [2021-03-27 Sat 22:25]
    :PROPERTIES:
    :ESTIMATED: 4.5
    :ACTUAL:   3.92
    :OWNER:    bhavin192
    :ID:       READ.1614970632
    :TASKID:   READ.1614970632
    :END:
    :LOGBOOK:
    CLOCK: [2021-03-27 Sat 22:15]--[2021-03-27 Sat 22:25] =>  0:10
    CLOCK: [2021-03-27 Sat 20:54]--[2021-03-27 Sat 21:35] =>  0:41
    CLOCK: [2021-03-27 Sat 20:08]--[2021-03-27 Sat 20:45] =>  0:37
    CLOCK: [2021-03-27 Sat 20:02]--[2021-03-27 Sat 20:03] =>  0:01
    CLOCK: [2021-03-26 Fri 21:53]--[2021-03-26 Fri 22:10] =>  0:17
    CLOCK: [2021-03-25 Thu 21:47]--[2021-03-25 Thu 22:03] =>  0:16
    CLOCK: [2021-03-25 Thu 20:34]--[2021-03-25 Thu 21:07] =>  0:33
    CLOCK: [2021-03-24 Wed 21:46]--[2021-03-24 Wed 22:10] =>  0:24
    CLOCK: [2021-03-24 Wed 20:32]--[2021-03-24 Wed 21:02] =>  0:30
    CLOCK: [2021-03-23 Tue 21:38]--[2021-03-23 Tue 22:04] =>  0:26
    :END:
    https://fosdem.org/2021/
    - [X] [[https://fosdem.org/2021/schedule/event/iacdriftpokemon/][Infrastructure-as-code drifts aren't like Pokemon : you can't catch em all]]  (30m)
    - [X] [[https://fosdem.org/2021/schedule/event/k8sconfigmgmtlandscape/][Kubernetes Config Management Landscape]]                                      (30m)
    - [X] [[https://fosdem.org/2021/schedule/event/greatcloudmigrationnetworkmeshauto/][The Great Cloud Migration with Network Automation & Service Mesh]]            (30m)
    - [X] [[https://fosdem.org/2021/schedule/event/yourmanagementlayershouldbecattletoo/][your management layer should be cattle too]]                                  (30m)
    - [X] [[https://fosdem.org/2021/schedule/event/principlespatternspracticeseffectiveiac/][Principles, Patterns, and Practices for Effective Infrastructure as Code]]    (30m)
    - [X] [[https://fosdem.org/2021/schedule/event/configconfigeverywhere/][Config, config everywhere]]                                                   (30m)
    - [X] [[https://fosdem.org/2021/schedule/event/clusterapiascode/][Cluster API as Code]]                                                         (30m)
    - [X] [[https://fosdem.org/2021/schedule/event/scalingreleasemgmtgitops/][Scaling Release Management with GitOps]]                                      (30m)
    - [X] [[https://fosdem.org/2021/schedule/event/desktoplinuxmgmtatfacebook/][Desktop Linux Management at Facebook]]                                        (30m)
*** DONE Watch FOSDEM 2021 - Go - Part I [4/4]
    CLOSED: [2021-03-27 Sat 23:52]
    :PROPERTIES:
    :ESTIMATED: 2
    :ACTUAL:   1.40
    :OWNER:    bhavin192
    :ID:       READ.1614970632
    :TASKID:   READ.1614970632
    :END:
    :LOGBOOK:
    CLOCK: [2021-03-27 Sat 22:28]--[2021-03-27 Sat 23:52] =>  1:24
    :END:
    https://fosdem.org/2021/
    - [X] [[https://fosdem.org/2021/schedule/event/goserverinbrowser/][Deploy a Go HTTP server in your browser]]                                                     (30m)
    - [X] [[https://fosdem.org/2021/schedule/event/gowithoutwires/][Go Without Wires]]                                                                            (30m)
    - [X] [[https://fosdem.org/2021/schedule/event/gowebrtc/][Drones, Virtual Reality and Multiplayer NES Games. The fun you can have with Pion WebRTC!]]   (30m)
    - [X] [[https://fosdem.org/2021/schedule/event/gopython/][Calling Python from Go In Memory]]                                                            (30m)
** gandalfdwite
*** DONE Programming Kubernetes (OReilly) [3/3]
    CLOSED: [2021-03-25 Thu 12:09]
    :PROPERTIES:
    :ESTIMATED: 12
    :ACTUAL:   12.53
    :OWNER: gandalfdwite
    :ID: READ.1614350576
    :TASKID: READ.1614350576
    :END:
    :LOGBOOK:
    CLOCK: [2021-03-24 Wed 07:30]--[2021-03-24 Wed 08:15] =>  0:45
    CLOCK: [2021-03-23 Tue 20:00]--[2021-03-23 Tue 21:05] =>  1:05
    CLOCK: [2021-03-22 Mon 18:00]--[2021-03-22 Mon 19:00] =>  1:00
    CLOCK: [2021-03-21 Sun 13:00]--[2021-03-21 Sun 15:11] =>  2:11
    CLOCK: [2021-03-20 Sat 10:30]--[2021-03-20 Sat 11:50] =>  1:20
    CLOCK: [2021-03-19 Fri 23:20]--[2021-03-20 Sat 00:25] =>  1:05
    CLOCK: [2021-03-18 Thu 17:15]--[2021-03-18 Thu 18:55] =>  1:40
    CLOCK: [2021-03-17 Wed 20:30]--[2021-03-17 Wed 21:35] =>  1:05
    CLOCK: [2021-03-16 Tue 21:00]--[2021-03-16 Tue 22:00] =>  1:00
    CLOCK: [2021-03-14 Sun 10:50]--[2021-03-14 Sun 12:11] =>  1:21
    :END:
    - [X] Automating Code gen           ( 4h )
    - [X] Solutions for Operators       ( 4h )
    - [X] Shipping Controllers          ( 4h )

*** DONE write blog post on how K8s scheduler works [1/1]
    CLOSED: [2021-03-29 Mon 12:10]
    :PROPERTIES:
    :ESTIMATED: 2
    :ACTUAL:   2.67
    :OWNER: gandalfdwite
    :ID: WRITE.1615902886
    :TASKID: WRITE.1615902886
    :END:
    :LOGBOOK:
    CLOCK: [2021-03-27 Sat 20:55]--[2021-03-27 Sat 21:45] =>  0:50
    CLOCK: [2021-03-25 Thu 17:10]--[2021-03-25 Thu 19:00] =>  1:50
    :END:
    - [X] Blog post on K8s scheduler works ( 2h )
** yudocaa
*** Introduction to Computer Systems
    :PROPERTIES:
    :ESTIMATED: 6
    :ACTUAL:
    :OWNER: yudocaa
    :ID: READ.1615954851
    :TASKID: READ.1615954851
    :END:
    :LOGBOOK:
    CLOCK: [2021-03-26 Fri 19:43]--[2021-03-26 Fri 21:08] =>  1:25
    CLOCK: [2021-03-27 Sat 20:38]--[2021-03-27 Sat 22:20] =>  1:42
    :END:
    - [X] Lecture 02: Bits, Bytes and Integers                            (2h)
    - [X] Lecture 03: Bits, Bytes, and Integers (Contd.)                  (2h)

