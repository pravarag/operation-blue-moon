#+TITLE: February 26 - March 13, 2021 (16 days)
#+PROPERTY: Effort_ALL 0 0:05 0:10 0:30 1:00 2:00 3:00 4:00
#+COLUMNS: %35ITEM %TASKID %OWNER %3PRIORITY %TODO %5ESTIMATED{+} %3ACTUAL{+}
* REPORTS
** SCRUM BOARD
#+BEGIN: block-update-board
#+END:
** DEVELOPER SUMMARY
#+BEGIN: block-update-summary
#+END:
** BURNDOWN CHART
#+BEGIN: block-update-graph
#+END:
** BURNDOWN LIST
#+PLOT: title:"Burndown" ind:1 deps:(3 4) set:"term dumb" set:"xtics scale 0.5" set:"ytics scale 0.5" file:"burndown.plt" set:"xrange [0:17]"
#+BEGIN: block-update-burndown
#+END:
** TASK LIST
#+BEGIN: columnview :hlines 2 :maxlevel 5 :id "TASKS"
#+END:
* TASKS
  :PROPERTIES:
  :ID:       TASKS
  :SPRINTLENGTH: 16
  :SPRINTSTART: <2021-02-26 Fri>
  :wpd-akshay196: 1
  :wpd-bhavin192: 1
  :wpd-gandalfdwite: 1
  :END:
** akshay196
*** DONE Solve Go Examples - Part V
    CLOSED: [2021-03-13 Sat 11:00]
    :PROPERTIES:
    :ESTIMATED: 16
    :ACTUAL:   11.05
    :OWNER: akshay196
    :ID: DEV.1609175417
    :TASKID: DEV.1609175417
    :END:
    :LOGBOOK:
    CLOCK: [2021-03-13 Sat 09:47]--[2021-03-13 Sat 11:00] =>  1:13
    CLOCK: [2021-03-12 Fri 08:14]--[2021-03-12 Fri 09:01] =>  0:47
    CLOCK: [2021-03-11 Thu 16:04]--[2021-03-11 Thu 16:50] =>  0:46
    CLOCK: [2021-03-11 Thu 08:44]--[2021-03-11 Thu 09:59] =>  1:15
    CLOCK: [2021-03-10 Wed 08:29]--[2021-03-10 Wed 09:38] =>  1:09
    CLOCK: [2021-03-09 Tue 08:13]--[2021-03-09 Tue 09:25] =>  1:12
    CLOCK: [2021-03-06 Sat 08:35]--[2021-03-06 Sat 08:48] =>  0:13
    CLOCK: [2021-03-04 Thu 07:56]--[2021-03-04 Thu 09:12] =>  1:16
    CLOCK: [2021-03-03 Wed 08:06]--[2021-03-03 Wed 08:58] =>  0:52
    CLOCK: [2021-03-02 Tue 08:13]--[2021-03-02 Tue 09:07] =>  0:54
    CLOCK: [2021-03-01 Mon 07:59]--[2021-03-01 Mon 08:56] =>  0:57
    CLOCK: [2021-02-27 Sat 08:54]--[2021-02-27 Sat 09:23] =>  0:29
    :END:
    - [X] 16-slices                                   (3h)
    - [X] 17-project-empty-file-finder                (3h)
    - [X] 18-project-bouncing-ball                    (3h)
** bhavin192
*** DONE Site Reliability Engineering - IV. Management - Part I [1/1]
    CLOSED: [2021-03-04 Thu 20:41]
    :PROPERTIES:
    :ESTIMATED: 6
    :ACTUAL:   4.70
    :OWNER:    bhavin192
    :ID:       READ.1596455869
    :TASKID:   READ.1596455869
    :END:
    :LOGBOOK:
    CLOCK: [2021-03-04 Thu 20:18]--[2021-03-04 Thu 20:41] =>  0:23
    CLOCK: [2021-03-03 Wed 21:47]--[2021-03-03 Wed 22:03] =>  0:16
    CLOCK: [2021-03-03 Wed 20:30]--[2021-03-03 Wed 21:10] =>  0:40
    CLOCK: [2021-03-02 Tue 21:52]--[2021-03-02 Tue 22:05] =>  0:13
    CLOCK: [2021-03-02 Tue 20:51]--[2021-03-02 Tue 21:11] =>  0:20
    CLOCK: [2021-03-02 Tue 20:16]--[2021-03-02 Tue 20:48] =>  0:32
    CLOCK: [2021-03-01 Mon 21:53]--[2021-03-01 Mon 22:00] =>  0:07
    CLOCK: [2021-03-01 Mon 20:09]--[2021-03-01 Mon 21:02] =>  0:53
    CLOCK: [2021-02-28 Sun 19:29]--[2021-02-28 Sun 20:33] =>  1:04
    CLOCK: [2021-02-27 Sat 20:42]--[2021-02-27 Sat 20:56] =>  0:14
    :END:
    https://landing.google.com/sre/sre-book/toc/index.html
    - [X] 28. Accelerating SREs to On-Call and Beyond   (6h)
*** DONE FOSDEM 2021 - Continuous Integration and Continuous Deployment [13/13]
    CLOSED: [2021-03-13 Sat 15:05]
    :PROPERTIES:
    :ESTIMATED: 8
    :ACTUAL:   5.78
    :OWNER:    bhavin192
    :ID:       READ.1614970632
    :TASKID:   READ.1614970632
    :END:
    :LOGBOOK:
    CLOCK: [2021-03-13 Sat 14:49]--[2021-03-13 Sat 15:05] =>  0:16
    CLOCK: [2021-03-11 Thu 22:34]--[2021-03-11 Thu 22:47] =>  0:13
    CLOCK: [2021-03-11 Thu 20:29]--[2021-03-11 Thu 20:42] =>  0:13
    CLOCK: [2021-03-10 Wed 21:58]--[2021-03-10 Wed 22:23] =>  0:25
    CLOCK: [2021-03-09 Tue 22:35]--[2021-03-09 Tue 22:56] =>  0:21
    CLOCK: [2021-03-09 Tue 20:22]--[2021-03-09 Tue 21:10] =>  0:48
    CLOCK: [2021-03-08 Mon 23:42]--[2021-03-08 Mon 23:53] =>  0:11
    CLOCK: [2021-03-08 Mon 20:43]--[2021-03-08 Mon 21:16] =>  0:33
    CLOCK: [2021-03-07 Sun 16:04]--[2021-03-07 Sun 17:24] =>  1:20
    CLOCK: [2021-03-06 Sat 15:32]--[2021-03-06 Sat 16:59] =>  1:27
    :END:
    https://fosdem.org/2021/
    - [X] [[https://fosdem.org/2021/schedule/event/the_road_to_interoperability_in_cicd/][The Road to Interoperability in CI/CD]]                                       (25m)
    - [X] [[https://fosdem.org/2021/schedule/event/combining_progressive_delivery_with_gitops_and_continuous_delivery/][Combining Progressive Delivery With GitOps And Continuous Delivery]]          (50m)
    - [X] [[https://fosdem.org/2021/schedule/event/events_in_cicd/][Events in CI/CD]]                                                             (25m)
    - [X] [[https://fosdem.org/2021/schedule/event/look_ma_no_hands_jenkins_testability_and_monitoring/][Who watches the watchers - a Jenkins journey]]                                (50m)
    - [X] [[https://fosdem.org/2021/schedule/event/improving_the_developer_experience_of_infrastructure_as_code_and_gitops/][Improving the Developer Experience of Infrastructure as Code and GitOps]]     (50m)
    - [X] [[https://fosdem.org/2021/schedule/event/kuberig_kubernetes_without_yaml/][Kuberig, Kubernetes without the YAML burn-out!]]                              (20m)
    - [X] [[https://fosdem.org/2021/schedule/event/collecting_and_visualizing_continuous_delivery_indicators/][Collecting and visualizing Continuous Delivery Indicators]]                   (50m)
    - [X] [[https://fosdem.org/2021/schedule/event/stairstep_your_kubernetes_deployment_workflow_with_gimlet_and_gitops/][Stairstep your Kubernetes deployment workflow with Gimlet and GitOps]]        (20m)
    - [X] [[https://fosdem.org/2021/schedule/event/putting_chaos_into_continuous_delivery/][Putting Chaos into Continuous Delivery]]                                      (50m)
    - [X] [[https://fosdem.org/2021/schedule/event/ci_on_gitlab_ringing_gitlab_tekton_and_prow_together/][CI on Gitlab. Bringing Gitlab, Tekton and Prow together (with some magic)]]   (50m)
    - [X] [[https://fosdem.org/2021/schedule/event/configure_once_run_everywhere/][Configure Once, Run Everywhere]]                                              (50m)
    - [X] [[https://fosdem.org/2021/schedule/event/identifying_performance_changes_using_peass/][Identifying Performance Changes Using Peass]]                                 (20m)
    - [X] [[https://fosdem.org/2021/schedule/event/gitops_working_group/][GitOps Working Group - Overview and Invitation]]                              (20m)
*** DONE FOSDEM 2021 - Containers - Part I [4/4]
    CLOSED: [2021-03-13 Sat 18:31]
    :PROPERTIES:
    :ESTIMATED: 2
    :ACTUAL:   1.73
    :OWNER:    bhavin192
    :ID:       READ.1614970632
    :TASKID:   READ.1614970632
    :END:
    :LOGBOOK:
    CLOCK: [2021-03-13 Sat 16:49]--[2021-03-13 Sat 18:31] =>  1:42
    CLOCK: [2021-03-13 Sat 15:05]--[2021-03-13 Sat 15:07] =>  0:02
    :END:
    https://fosdem.org/2021/
    - [X] [[https://fosdem.org/2021/schedule/event/containers_ebpf_kernel/][Advanced BPF kernel features for the container age]]                  (35m)
    - [X] [[https://fosdem.org/2021/schedule/event/containers_k8s_docker/][Docker Is No More! What Now?]]                                        (35m)
    - [X] [[https://fosdem.org/2021/schedule/event/containers_k8s_dbaas/][Hybrid Cloud Open Source DBaaS with Kubernetes]]                      (25m)
    - [X] [[https://fosdem.org/2021/schedule/event/containers_k8s_seccomp_notify/][Seccomp Notify on Kubernetes]]                                        (25m)
** gandalfdwite
*** DONE Oreilly - Kubernetes Operator online Training videos (recorded) [1/1]
    CLOSED: [2021-02-28 Sun 16:20]
    :PROPERTIES:
    :ESTIMATED: 3
    :ACTUAL:
    :OWNER: gandalfdwite
    :ID: DEV.1608137469
    :TASKID: DEV.1608137469
    :END:
    :LOGBOOK:
    CLOCK: [2021-02-26 Fri 22:00]--[2021-02-27 Sat 01:00] =>  3:00
    :END:
    - [X] Watch Kubernetes Operator training video  ( 3h )
*** DONE Programming Kubernetes (OReilly) [4/4]
    CLOSED: [2021-03-14 Sun 17:30]
    :PROPERTIES:
    :ESTIMATED: 13
    :ACTUAL:   13.25
    :OWNER: gandalfdwite
    :ID: READ.1614350576
    :TASKID: READ.1614350576
    :END:
    :LOGBOOK:
    CLOCK: [2021-03-12 Fri 16:10]--[2021-03-12 Fri 17:30] =>  1:20
    CLOCK: [2021-03-11 Thu 20:40]--[2021-03-11 Thu 21:45] =>  1:05
    CLOCK: [2021-03-10 Wed 19:00]--[2021-03-10 Wed 20:30] =>  1:30
    CLOCK: [2021-03-09 Tue 21:45]--[2021-03-09 Tue 23:00] =>  1:15
    CLOCK: [2021-03-08 Mon 18:30]--[2021-03-08 Mon 19:30] =>  1:00
    CLOCK: [2021-03-07 Sun 13:10]--[2021-03-07 Sun 14:30] =>  1:20
    CLOCK: [2021-03-06 Sat 16:45]--[2021-03-06 Sat 18:00] =>  1:15
    CLOCK: [2021-03-05 Fri 21:30]--[2021-03-05 Fri 22:30] =>  1:00
    CLOCK: [2021-03-03 Wed 22:50]--[2021-03-04 Thu 00:00] =>  1:10
    CLOCK: [2021-03-02 Tue 20:00]--[2021-03-02 Tue 21:00] =>  1:00
    CLOCK: [2021-02-28 Sun 11:30]--[2021-02-28 Sun 12:50] =>  1:20
    :END:
    - [X] Introduction                  ( 2h )
    - [X] Kubernetes API Basics         ( 3h )
    - [X] Basics of client-go           ( 4h )
    - [X] Using Custom Resources        ( 4h )
