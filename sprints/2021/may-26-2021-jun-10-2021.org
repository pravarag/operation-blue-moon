#+TITLE: May 26-June 10, 2021 (16 days)
#+PROPERTY: Effort_ALL 0 0:05 0:10 0:30 1:00 2:00 3:00 4:00
#+COLUMNS: %35ITEM %TASKID %OWNER %3PRIORITY %TODO %5ESTIMATED{+} %3ACTUAL{+}
* REPORTS
** SCRUM BOARD
#+BEGIN: block-update-board
#+END:
** DEVELOPER SUMMARY
#+BEGIN: block-update-summary
#+END:
** BURNDOWN CHART
#+BEGIN: block-update-graph
#+END:
** BURNDOWN LIST
#+PLOT: title:"Burndown" ind:1 deps:(3 4) set:"term dumb" set:"xtics scale 0.5" set:"ytics scale 0.5" file:"burndown.plt" set:"xrange [0:17]"
#+BEGIN: block-update-burndown
#+END:
** TASK LIST
#+BEGIN: columnview :hlines 2 :maxlevel 5 :id "TASKS"
#+END:
* TASKS
  :PROPERTIES:
  :ID:       TASKS
  :SPRINTLENGTH: 16
  :SPRINTSTART: <2021-05-26 Wed>
  :wpd-akshay196: 1
  :wpd-bhavin192: 1
  :END:
** akshay196
*** DONE Develop an IRC client in Go - Part II
    CLOSED: [2021-06-10 Thu 20:45]
    :PROPERTIES:
    :ESTIMATED: 16
    :ACTUAL:   12.48
    :OWNER:    akshay196
    :ID:       DEV.1619594108
    :TASKID:   DEV.1619594108
    :END:
    :LOGBOOK:
    CLOCK: [2021-06-10 Thu 18:55]--[2021-06-10 Thu 20:45] =>  1:50
    CLOCK: [2021-06-09 Wed 20:41]--[2021-06-09 Wed 21:30] =>  0:49
    CLOCK: [2021-06-08 Tue 16:21]--[2021-06-08 Tue 17:32] =>  1:11
    CLOCK: [2021-06-07 Mon 20:22]--[2021-06-07 Mon 21:33] =>  1:11
    CLOCK: [2021-06-06 Sun 19:10]--[2021-06-06 Sun 20:36] =>  1:26
    CLOCK: [2021-06-05 Sat 21:49]--[2021-06-05 Sat 22:40] =>  0:51
    CLOCK: [2021-06-04 Fri 18:48]--[2021-06-04 Fri 20:25] =>  1:37
    CLOCK: [2021-06-03 Thu 07:44]--[2021-06-03 Thu 08:28] =>  0:44
    CLOCK: [2021-06-01 Tue 19:32]--[2021-06-01 Tue 20:42] =>  1:10
    CLOCK: [2021-05-31 Mon 21:27]--[2021-05-31 Mon 22:12] =>  0:45
    CLOCK: [2021-05-29 Sat 19:25]--[2021-05-29 Sat 20:20] =>  0:55
    :END:
    - [X] Write a code: connect to a server
** bhavin192
*** DONE Production Kubernetes - 1. A Path to Production
    CLOSED: [2021-05-30 Sun 18:51]
    :PROPERTIES:
    :ESTIMATED: 4.5
    :ACTUAL:   4.13
    :OWNER:    bhavin192
    :ID:       READ.1622393322
    :TASKID:   READ.1622393322
    :END:
    :LOGBOOK:
    CLOCK: [2021-05-30 Sun 18:42]--[2021-05-30 Sun 18:51] =>  0:09
    CLOCK: [2021-05-30 Sun 18:26]--[2021-05-30 Sun 18:39] =>  0:13
    CLOCK: [2021-05-30 Sun 17:59]--[2021-05-30 Sun 18:04] =>  0:05
    CLOCK: [2021-05-30 Sun 17:42]--[2021-05-30 Sun 17:48] =>  0:06
    CLOCK: [2021-05-30 Sun 17:21]--[2021-05-30 Sun 17:36] =>  0:15
    CLOCK: [2021-05-30 Sun 17:05]--[2021-05-30 Sun 17:17] =>  0:12
    CLOCK: [2021-05-30 Sun 16:46]--[2021-05-30 Sun 17:00] =>  0:14
    CLOCK: [2021-05-30 Sun 16:31]--[2021-05-30 Sun 16:41] =>  0:10
    CLOCK: [2021-05-30 Sun 16:12]--[2021-05-30 Sun 16:23] =>  0:11
    CLOCK: [2021-05-30 Sun 15:59]--[2021-05-30 Sun 16:09] =>  0:10
    CLOCK: [2021-05-30 Sun 15:34]--[2021-05-30 Sun 15:53] =>  0:19
    CLOCK: [2021-05-30 Sun 13:56]--[2021-05-30 Sun 14:01] =>  0:05
    CLOCK: [2021-05-30 Sun 13:32]--[2021-05-30 Sun 13:43] =>  0:11
    CLOCK: [2021-05-30 Sun 12:19]--[2021-05-30 Sun 12:29] =>  0:10
    CLOCK: [2021-05-29 Sat 22:20]--[2021-05-29 Sat 22:43] =>  0:23
    CLOCK: [2021-05-29 Sat 20:51]--[2021-05-29 Sat 21:10] =>  0:19
    CLOCK: [2021-05-26 Wed 21:03]--[2021-05-26 Wed 21:05] =>  0:02
    CLOCK: [2021-05-26 Wed 20:29]--[2021-05-26 Wed 21:03] =>  0:34
    CLOCK: [2021-05-26 Wed 20:17]--[2021-05-26 Wed 20:29] =>  0:12
    CLOCK: [2021-05-26 Wed 20:09]--[2021-05-26 Wed 20:17] =>  0:08
    :END:
    https://learning.oreilly.com/library/view/production-kubernetes/9781492092292/
*** DONE Production Kubernetes - 2. Deployment Models
    CLOSED: [2021-06-06 Sun 17:18]
    :PROPERTIES:
    :ESTIMATED: 8
    :ACTUAL:   7.38
    :OWNER:    bhavin192
    :ID:       READ.1622393322
    :TASKID:   READ.1622393322
    :END:
    :LOGBOOK:
    CLOCK: [2021-06-06 Sun 17:12]--[2021-06-06 Sun 17:18] =>  0:06
    CLOCK: [2021-06-06 Sun 16:29]--[2021-06-06 Sun 17:09] =>  0:40
    CLOCK: [2021-06-06 Sun 15:53]--[2021-06-06 Sun 16:21] =>  0:28
    CLOCK: [2021-06-06 Sun 13:51]--[2021-06-06 Sun 14:09] =>  0:18
    CLOCK: [2021-06-06 Sun 13:24]--[2021-06-06 Sun 13:38] =>  0:14
    CLOCK: [2021-06-06 Sun 13:09]--[2021-06-06 Sun 13:17] =>  0:08
    CLOCK: [2021-06-05 Sat 17:35]--[2021-06-05 Sat 18:06] =>  0:31
    CLOCK: [2021-06-05 Sat 16:57]--[2021-06-05 Sat 17:27] =>  0:30
    CLOCK: [2021-06-05 Sat 15:26]--[2021-06-05 Sat 16:01] =>  0:35
    CLOCK: [2021-06-04 Fri 20:19]--[2021-06-04 Fri 20:42] =>  0:23
    CLOCK: [2021-06-04 Fri 19:51]--[2021-06-04 Fri 19:59] =>  0:08
    CLOCK: [2021-06-03 Thu 20:45]--[2021-06-03 Thu 20:50] =>  0:05
    CLOCK: [2021-06-03 Thu 20:02]--[2021-06-03 Thu 20:45] =>  0:43
    CLOCK: [2021-06-03 Thu 19:44]--[2021-06-03 Thu 19:57] =>  0:13
    CLOCK: [2021-06-03 Thu 19:33]--[2021-06-03 Thu 19:34] =>  0:01
    CLOCK: [2021-06-02 Wed 22:11]--[2021-06-02 Wed 22:40] =>  0:29
    CLOCK: [2021-06-01 Tue 19:35]--[2021-06-01 Tue 20:35] =>  1:00
    CLOCK: [2021-05-31 Mon 20:14]--[2021-05-31 Mon 21:05] =>  0:51
    :END:
    https://learning.oreilly.com/library/view/production-kubernetes/9781492092292/
*** DONE Write blog post about removing commenting system
    CLOSED: [2021-06-10 Sun 20:11]
    :PROPERTIES:
    :ESTIMATED: 3.5
    :ACTUAL:   3.43
    :OWNER:    bhavin192
    :ID:       WRITE.1623001691
    :TASKID:   WRITE.1623001691
    :END:
    :LOGBOOK:
    CLOCK: [2021-06-10 Thu 19:51]--[2021-06-10 Thu 20:11] =>  0:20
    CLOCK: [2021-06-09 Wed 21:10]--[2021-06-09 Wed 21:15] =>  0:05
    CLOCK: [2021-06-09 Wed 20:39]--[2021-06-09 Wed 21:09] =>  0:30
    CLOCK: [2021-06-09 Wed 19:52]--[2021-06-09 Wed 20:30] =>  0:38
    CLOCK: [2021-06-08 Tue 20:01]--[2021-06-08 Tue 21:02] =>  1:01
    CLOCK: [2021-06-08 Tue 19:23]--[2021-06-08 Tue 19:50] =>  0:27
    CLOCK: [2021-06-07 Mon 21:20]--[2021-06-07 Mon 21:32] =>  0:12
    CLOCK: [2021-06-07 Mon 20:44]--[2021-06-07 Mon 20:57] =>  0:13
    :END:
