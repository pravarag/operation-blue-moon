#+TITLE: April 27-May 12, 2021 (16 days)
#+PROPERTY: Effort_ALL 0 0:05 0:10 0:30 1:00 2:00 3:00 4:00
#+COLUMNS: %35ITEM %TASKID %OWNER %3PRIORITY %TODO %5ESTIMATED{+} %3ACTUAL{+}
* REPORTS
** SCRUM BOARD
#+BEGIN: block-update-board
#+END:
** DEVELOPER SUMMARY
#+BEGIN: block-update-summary
#+END:
** BURNDOWN CHART
#+BEGIN: block-update-graph
#+END:
** BURNDOWN LIST
#+PLOT: title:"Burndown" ind:1 deps:(3 4) set:"term dumb" set:"xtics scale 0.5" set:"ytics scale 0.5" file:"burndown.plt" set:"xrange [0:17]"
#+BEGIN: block-update-burndown
#+END:
** TASK LIST
#+BEGIN: columnview :hlines 2 :maxlevel 5 :id "TASKS"
#+END:
* TASKS
  :PROPERTIES:
  :ID:       TASKS
  :SPRINTLENGTH: 16
  :SPRINTSTART: <2021-04-27 Tue>
  :wpd-akshay196: 1
  :END:
** akshay196
*** DONE Solve Go Examples - Part VIII
    CLOSED: [2021-04-30 Fri 09:55]
    :PROPERTIES:
    :ESTIMATED:  6
    :ACTUAL:   4.87
    :OWNER: akshay196
    :ID: DEV.1609175417
    :TASKID: DEV.1609175417
    :END:
    :LOGBOOK:
    CLOCK: [2021-04-30 Fri 08:03]--[2021-04-30 Fri 09:55] =>  1:52
    CLOCK: [2021-04-29 Thu 07:10]--[2021-04-29 Thu 08:55] =>  1:45
    CLOCK: [2021-04-28 Wed 08:05]--[2021-04-28 Wed 09:20] =>  1:15
    :END:
    - [X] advfuncs                           (6h)
*** DONE Develop an IRC client in Go - Part I
    CLOSED: [2021-05-08 Sat 09:05]
    :PROPERTIES:
    :ESTIMATED: 10
    :ACTUAL:   6.58
    :OWNER: akshay196
    :ID: DEV.1619594108
    :TASKID: DEV.1619594108
    :END:
    :LOGBOOK:
    CLOCK: [2021-05-08 Sat 07:48]--[2021-05-08 Sat 09:05] =>  1:17
    CLOCK: [2021-05-07 Fri 07:27]--[2021-05-07 Fri 08:59] =>  1:32
    CLOCK: [2021-05-04 Tue 08:23]--[2021-05-04 Tue 09:01] =>  0:38
    CLOCK: [2021-05-02 Sun 21:47]--[2021-05-02 Sun 22:22] =>  0:35
    CLOCK: [2021-04-30 Fri 20:19]--[2021-04-30 Fri 20:55] =>  0:36
    CLOCK: [2021-04-29 Thu 19:09]--[2021-04-29 Thu 21:06] =>  1:57
    :END:
    - [X] Read [[https://tools.ietf.org/html/rfc2812][IRC protocol specification]]
