README
======

"Operation Blue Moon" contains Org files for managing project tasks.

Motivation
==========

* Getting Things Done
* Time management
* Better estimation of work
* Self-improvement
* Statistics

Workflow
========

If you are interested in participating in these sprints, define your
long-term (3-5 years) and short-term goals (3-6 months), and ping
"mbuf" on irc.libera.chat.

### Why

Long-term goals could be your passion or job roles that you want to
pursue such as (but not limited to):

* DevSecOps Engineer
* Security Engineer
* Kernel Engineer
* Systems Programmer
* Web developer
* UI/UX Designer
* Writer
* Document Translator
* Data Engineer
* Data Scientist
* Packager
* Application Developer
* QA Tester

### What

The short-term goals may include the technology and skills that you
need to learn to achieve your long-term goals. The short-term goals are
further sub-divided into tasks which are aligned to sprints.

The plan/ directory has sub-directories for the long-term goals. You
should create an Org file for your name, and place it under the
respective long-term goal that you plan to work towards. A
user-example.org is available in the plan/ directory for reference.

The tasks can be any one of the following categories: READ, WRITE,
DEV, OPS, EVENT, MEETING.

You can insert a new task into your Org file by invoking "M-x
insert-org-entry" after adding the following function to your Emacs
init configuration:

    (define-skeleton insert-org-entry
      "Prompt for task, estimate and category"
      nil
      '(setq task  (skeleton-read "Task: "))
      '(setq estimate  (skeleton-read "Estimate: "))
      '(setq category (skeleton-read "Category: "))
      '(setq timestamp (format-time-string "%s"))
      "** " task \n
      ":PROPERTIES:" \n
      ":ESTIMATED: " estimate \n
      ":ACTUAL:" \n
      ":OWNER: shaks" \n
      ":ID: " category "." timestamp \n
      ":TASKID: " category "." timestamp \n
      ":END:")

A task that spans across multiple sprints should have the same ID and
TASKID, but, you can add a suffix to the task name as "Part I", "Part
II", "Part III" etc. across sprints. This helps to track the total
time spent on a task.

### When

For each sprint, you list the tasks that you want to work under the
PLAN heading in your Org file.

You should also decide on the hours you can spend per day and create a
work-per-day entry for each sprint in the PROPERTIES drawer as shown
below:

    ** October   10, 2018 - October  23, 2018 (14 days)
       :PROPERTIES:
       :wpd-mbuf: 2
       :END:

You should not clock anything in your Org file. All clocked timings
will be in sprints/.

### How

You need to break the tasks and sub-tasks in detail with checkboxes
and links to documentation or references.

During the sprint, you can send PR with your clocked timings. You need
to ensure that ACTUALS in the PROPERTIES drawer get updated. You can
add the following to your Emacs configuration:

    (defun org-update-actuals ()
      (interactive)
      (org-entry-put (point) "ACTUAL"
                     (format "%0.2f" (/ (org-clock-sum-current-item) 60.0))))

    (global-set-key (kbd "C-c s c") 'org-update-actuals)

At the end of a sprint, a retrospection meeting will be held to review
the same.

It is recommended for participants to wear a heavy watch so that they
have the notion of time.

Upcoming Sprints
================

* February   16, 2022 - March      3, 2022 (16 days)
* March       4, 2022 - March     17, 2022 (14 days)
* March      18, 2022 - April      1, 2022 (15 days)
* April       2, 2022 - April     15, 2022 (14 days)
* April      16, 2022 - May        1, 2022 (16 days)
* May         2, 2022 - May       15, 2022 (14 days)
* May        16, 2022 - May       31, 2022 (16 days)
* June        1, 2022 - June      13, 2022 (13 days)
* June       14, 2022 - June      29, 2022 (16 days)
* June       30, 2022 - July      12, 2022 (13 days)
* July       13, 2022 - July      29, 2022 (17 days)
* July       30, 2022 - August    11, 2022 (13 days)
* August     12, 2022 - August    27, 2022 (16 days)
* August     28, 2022 - September  9, 2022 (13 days)
* September  10, 2022 - September 26, 2022 (17 days)
* September  27, 2022 - October    8, 2022 (12 days)
* October     9, 2022 - October   25, 2022 (17 days)
* October    26, 2022 - November   7, 2022 (13 days)
* November    8, 2022 - November  24, 2022 (17 days)
* November   25, 2022 - December   7, 2022 (13 days)
* December    8, 2022 - December  23, 2022 (16 days)
* December   24, 2022 - January    5, 2023 (13 days)
* January     6, 2023 - January   22, 2023 (17 days)
* January    23, 2023 - February   4, 2023 (13 days)
* February    5, 2023 - February  20, 2023 (16 days)
* February   21, 2023 - March      6, 2023 (14 days)

Contact
=======

Shakthi Kannan ("mbuf")

author AT shakthimaan dot com
